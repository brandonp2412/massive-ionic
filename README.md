# Running the app

First, install

- [Node.js](nodejs.org)
- [Ionic](https://ionicframework.com/docs/intro/cli)

Installing this projects dependencies:

```
npm install
```

Running on an android device:

```
ionic cordova run android --prod
```

Live reloading on an android device:

```
ionic cordova run android -l
```

Live reloading as a website:

```
ionic serve
```

# History

Each workout has a name, a number of reps, a weight and a unit.
The unit can be any string, here I didn't know the weight of the machine plates at my gym, so I've just specified plates.

<img src="screenshots/history.png" height="700">

# Charts

Charts measure total volume in a single day (reps x sets x weight).
Specifying the granularity filters results by the current week, month or year.

<img src="screenshots/charts.png" height="700">

# Sidebar

<img src="screenshots/sidebar.png" height="700">

# Workouts

A workout is a number of sets, and a list of drills.
You are expected to complete all drills for the number of sets today.

<img src="screenshots/workouts.png" height="700">

# Progress

The progress of a workout is the number of sets completed today, out of the total sets specified by the workout.

<img src="screenshots/progress.png" height="700">
