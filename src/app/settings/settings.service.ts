import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import { SettingsModel } from "./settings.model";

@Injectable({
  providedIn: "root",
})
export class SettingsService {
  constructor(private storage: Storage) {}

  async get(): Promise<SettingsModel> {
    let settings = await this.storage.get("settings");
    if (settings) return settings;
    settings = { darkMode: false };
    this.set(settings);
    return settings;
  }

  set(settings: SettingsModel): Promise<void> {
    return this.storage.set("settings", settings);
  }
}
