import { FormatModel } from "../format.model";

export class SettingsModel {
  darkMode = false;
  dateFormat: FormatModel = "short";
}
