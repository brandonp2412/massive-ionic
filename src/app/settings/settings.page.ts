import { Component, OnInit } from "@angular/core";
import { SettingsModel } from "./settings.model";
import { SettingsService } from "./settings.service";

@Component({
  selector: "app-settings",
  templateUrl: "./settings.page.html",
  styleUrls: ["./settings.page.scss"],
})
export class SettingsPage implements OnInit {
  settings = new SettingsModel();
  formatOptions = [
    "short",
    "medium",
    "long",
    "full",
    "shortDate",
    "mediumDate",
    "longDate",
    "fullDate",
    "shortTime",
    "mediumTime",
    "longTime",
    "fullTime",
  ];
  exampleDate = new Date("01/13/2020");

  constructor(private settingsService: SettingsService) {}

  async ngOnInit() {
    const settings = await this.settingsService.get();
    if (!settings) return;
    this.settings = settings;
  }

  async save() {
    await this.settingsService.set(this.settings);
    document.body.classList.toggle("dark", this.settings.darkMode);
  }
}
