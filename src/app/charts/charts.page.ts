import { Component, OnInit } from "@angular/core";
import * as Chart from "chart.js";
import * as moment from "moment";
import { DrillService } from "../drill/drill.service";

@Component({
  selector: "app-charts",
  templateUrl: "./charts.page.html",
  styleUrls: ["./charts.page.scss"],
})
export class ChartsPage implements OnInit {
  drill: string;
  drillOptions: string[] = [];
  granularity: "All" | moment.unitOfTime.StartOf = "All";

  constructor(private drillService: DrillService) {}

  async ngOnInit() {
    this.drillOptions = await this.drillService.names();
    this.drill = this.drillOptions[0];
    await this.createChart();
  }

  async createChart() {
    const { data, labels } = await this.drillService.charts(
      this.drill,
      this.granularity,
    );
    const element = document.getElementById("myChart") as HTMLCanvasElement;
    new Chart(element.getContext("2d"), {
      type: "line",
      data: {
        labels,
        datasets: [{ borderColor: "pink", label: this.drill, data }],
      },
    });
  }
}
