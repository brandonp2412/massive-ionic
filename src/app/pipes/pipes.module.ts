import { NgModule } from "@angular/core";
import { DaysSincePipe } from "./days-since.pipe";

@NgModule({
  declarations: [DaysSincePipe],
  exports: [DaysSincePipe],
})
export class PipesModule {}
