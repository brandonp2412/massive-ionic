import { Pipe, PipeTransform } from "@angular/core";
import * as moment from "moment";

@Pipe({
  name: "daysSince",
})
export class DaysSincePipe implements PipeTransform {
  transform(value: string): number {
    return moment().diff(moment(value), "days");
  }
}
