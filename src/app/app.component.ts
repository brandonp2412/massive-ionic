import { Component, OnInit } from "@angular/core";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { Platform } from "@ionic/angular";
import { SettingsService } from "./settings/settings.service";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: "History",
      url: "/history",
      icon: "list-outline",
    },
    {
      title: "Workouts",
      url: "/workouts",
      icon: "barbell-outline",
    },
    {
      title: "Charts",
      url: "/charts",
      icon: "bar-chart-outline",
    },
    {
      title: "Settings",
      url: "/settings",
      icon: "settings-outline",
    },
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private settingsService: SettingsService,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.overlaysWebView(false);
      this.splashScreen.hide();
    });
  }

  async ngOnInit() {
    const settings = await this.settingsService.get();
    document.body.classList.toggle("dark", settings.darkMode);
  }
}
