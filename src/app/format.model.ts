export type FormatModel =
  | "short"
  | "medium"
  | "long"
  | "full"
  | "shortDate"
  | "mediumDate"
  | "longDate"
  | "fullDate"
  | "shortTime"
  | "mediumTime"
  | "longTime"
  | "fullTime";
