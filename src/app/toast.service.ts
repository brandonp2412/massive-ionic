import { Injectable } from "@angular/core";
import { ToastController } from "@ionic/angular";

@Injectable({
  providedIn: "root",
})
export class ToastService {
  constructor(private controller: ToastController) {}

  async create(text: string) {
    const toast = await this.controller.create({
      message: text,
      duration: 3000,
    });
    await toast.present();
  }
}
