/**
 * A workout is a set of drills expected to be completed today.
 */
export class WorkoutModel {
  id: string;
  name: string;

  /**
   * How many sets each drill should be done before completion.
   */
  sets = 3;

  /**
   * Which drills should be done today.
   */
  drills: string[] = [];
}
