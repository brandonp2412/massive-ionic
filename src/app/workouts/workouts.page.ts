import { Component, OnInit } from "@angular/core";
import { Storage } from "@ionic/storage";
import { WorkoutModel } from "./workout.model";

@Component({
  selector: "app-workouts",
  templateUrl: "./workouts.page.html",
  styleUrls: ["./workouts.page.scss"],
})
export class WorkoutsPage implements OnInit {
  workouts: WorkoutModel[] = [];

  constructor(private storage: Storage) {}

  ngOnInit() {}

  async ionViewWillEnter() {
    this.workouts = await this.storage.get("workouts");
    if (this.workouts) return;
    this.storage.set("workouts", []);
    this.workouts = [];
  }

  trackWorkout(_index: number, workout: WorkoutModel) {
    return workout.id;
  }

  async remove(id: string) {
    const workouts: WorkoutModel[] = await this.storage.get("workouts");
    const filtered = workouts.filter((workout) => workout.id !== id);
    await this.storage.set("workouts", filtered);
    this.workouts = await this.storage.get("workouts");
  }
}
