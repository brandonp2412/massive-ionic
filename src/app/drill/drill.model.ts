export default class DrillModel {
  id: string;
  name: string;
  reps: number;
  weight: number;
  unit = "kg";
  date = new Date().toISOString();
}
