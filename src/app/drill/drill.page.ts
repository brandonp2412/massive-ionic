import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { IonSelect } from "@ionic/angular";
import DrillModel from "./drill.model";
import { DrillService } from "./drill.service";

@Component({
  selector: "app-drill",
  templateUrl: "./drill.page.html",
  styleUrls: ["./drill.page.scss"],
})
export class DrillPage implements OnInit {
  drill = new DrillModel();
  drillOptions: DrillModel[];
  selectedDrill: DrillModel;
  dontOpen = false;

  @ViewChild("selectName") selectName: IonSelect;

  constructor(
    private activatedRoute: ActivatedRoute,
    private drillService: DrillService,
    private router: Router,
  ) {}

  async ngOnInit() {
    const id = this.activatedRoute.snapshot.paramMap.get("id");
    if (id !== "new") return (this.drill = await this.drillService.find(id));
    const firstDrill = await this.drillService.first();
    if (!firstDrill) return;
    this.dontOpen = true;
    this.drill = firstDrill;
    delete this.drill.id;
    this.drill.date = new Date().toISOString();
  }

  async save() {
    if (this.drill.id) await this.drillService.update(this.drill);
    else await this.newDrill();
    await this.router.navigate(["/history"]);
  }

  async newDrill() {
    await this.drillService.add(this.drill);
  }

  async selectDrill() {
    this.drill = { ...this.selectedDrill };
    delete this.drill.id;
    this.drill.date = new Date().toISOString();
    this.dontOpen = true;
  }

  async setOptions() {
    this.drillOptions = await this.drillService.getOptions(this.drill.name);
    if (!this.drillOptions.length || this.drill.id) return;
    if (this.dontOpen) return (this.dontOpen = false);
    this.selectName.open();
  }

  compareWith(drillA: DrillModel, drillB: DrillModel) {
    return drillA.id === drillB.id;
  }

  trackOption(_index: number, option: DrillModel) {
    return option.id;
  }
}
