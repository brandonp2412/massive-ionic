import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BackgroundMode } from "@ionic-native/background-mode/ngx";
import { LocalNotifications } from "@ionic-native/local-notifications/ngx";
import { IonicModule } from "@ionic/angular";
import { DirectivesModule } from "../directives/directives.module";
import { DrillPageRoutingModule } from "./drill-routing.module";
import { DrillPage } from "./drill.page";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DrillPageRoutingModule,
    DirectivesModule,
  ],
  declarations: [DrillPage],
  providers: [LocalNotifications, BackgroundMode],
})
export class DrillPageModule {}
