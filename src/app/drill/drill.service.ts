import { Injectable } from "@angular/core";
import { FileChooser } from "@ionic-native/file-chooser/ngx";
import { FilePath } from "@ionic-native/file-path/ngx";
import { File } from "@ionic-native/file/ngx";
import { Storage } from "@ionic/storage";
import groupBy from "group-by";
import * as moment from "moment";
import { v4 as uuid } from "uuid";
import { ToastService } from "../toast.service";
import { DrillReportModel } from "../workout/drill-report.model";
import DrillModel from "./drill.model";

export const DRILLS_CHANGED = "drills-changed";

@Injectable({
  providedIn: "root",
})
export class DrillService {
  changeEvent = new Event(DRILLS_CHANGED);

  constructor(
    private storage: Storage,
    private file: File,
    private toast: ToastService,
    private fileChooser: FileChooser,
    private filePath: FilePath,
  ) {}

  async set(drills: DrillModel[]) {
    await this.storage.set("drills", drills);
  }

  async add(drill: DrillModel): Promise<void> {
    drill.id = uuid();
    const drills: DrillModel[] = await this.storage.get("drills");
    await this.storage.set("drills", [drill, ...drills]);
    document.dispatchEvent(this.changeEvent);
  }

  async update(newDrill: DrillModel) {
    const drills: DrillModel[] = await this.storage.get("drills");
    await this.storage.set(
      "drills",
      drills.map((oldDrill) => {
        if (oldDrill.id !== newDrill.id) return oldDrill;
        return newDrill;
      }),
    );
    document.dispatchEvent(this.changeEvent);
  }

  async get({
    limit,
    offset,
    term,
  }: {
    limit: number;
    offset: number;
    term?: string;
  }): Promise<DrillModel[]> {
    let drills: DrillModel[] = await this.storage.get("drills");
    if (!drills) {
      this.set([]);
      drills = [];
    }
    return drills
      .filter(
        (drill) =>
          !term || drill?.name?.toLowerCase().includes(term.toLowerCase()),
      )
      .slice(offset * limit, (offset + 1) * limit);
  }

  async remove(id: string): Promise<void> {
    const drills: DrillModel[] = await this.storage.get("drills");
    await this.storage.set(
      "drills",
      drills.filter((drill) => drill.id !== id),
    );
    document.dispatchEvent(this.changeEvent);
  }

  async find(id: string): Promise<DrillModel> {
    const drills: DrillModel[] = await this.storage.get("drills");
    return drills.find((drill) => drill.id === id);
  }

  async first() {
    const drills = await this.get({ limit: 1, offset: 0 });
    return drills[0];
  }

  async download() {
    const drills = await this.storage.get("drills");
    await this.file.writeFile(
      this.file.externalRootDirectory + "/Download",
      "drills.json",
      JSON.stringify(drills),
      {
        replace: true,
      },
    );
    await this.toast.create("Drills exported. Check downloads.");
  }

  async upload() {
    let uri = await this.fileChooser.open({ mime: "application/json" });
    uri = await this.filePath.resolveNativePath(uri);
    const path = uri.slice(0, uri.lastIndexOf("/"));
    const file = uri.slice(uri.lastIndexOf("/") + 1);
    const data = await this.file.readAsText(path, file);
    let drills: DrillModel[] = JSON.parse(data);
    drills = drills.map((drill) => ({ ...drill, id: uuid() }));
    await this.storage.set("drills", drills);
  }

  async names() {
    const drills: DrillModel[] = await this.storage.get("drills");
    return drills
      .map((drill) => drill.name)
      .filter((drill) => drill)
      .filter((value, index, array) => array.indexOf(value) === index);
  }

  async report(drillNames: string[]) {
    const drills: DrillModel[] = await this.storage.get("drills");
    const reports: { [name: string]: DrillReportModel } = {};
    drillNames.forEach(
      (drillName) => (reports[drillName] = { name: drillName, sets: 0 }),
    );
    drills
      .filter((drill) => drillNames.includes(drill.name))
      .filter((drill) => moment(drill.date).isSame(moment(), "day"))
      .forEach((drill) => {
        reports[drill.name] = {
          name: drill.name,
          sets: reports[drill.name] ? reports[drill.name].sets + 1 : 1,
        };
      });
    return Object.values(reports);
  }

  async charts(drillName: string, unit?: "All" | moment.unitOfTime.StartOf) {
    const drills: DrillModel[] = await this.storage.get("drills");
    const targetDrills = drills.filter((drill) => drill.name === drillName);
    const datasets: { [name: string]: number } = {};
    const labels: Set<string> = new Set();
    const groupedDrills: {
      [date: string]: DrillModel[];
    } = groupBy(targetDrills, (drill: DrillModel) =>
      moment(drill.date).format("YYYY-MM-DD"),
    );
    const dates = Object.keys(groupedDrills);
    dates
      .filter(
        (date) =>
          !unit || unit === "All" || moment(date).isSame(moment(), unit),
      )
      .sort((a, b) => new Date(a).getTime() - new Date(b).getTime())
      .forEach((date) => {
        groupedDrills[date].forEach((drill) => {
          datasets[date] = datasets[date]
            ? datasets[date] + drill.reps * drill.weight
            : drill.reps * drill.weight;
          labels.add(date);
        });
      });
    return { data: Object.values(datasets), labels: Array.from(labels) };
  }

  async getOptions(term?: string): Promise<DrillModel[]> {
    const drills: DrillModel[] = await this.storage.get("drills");
    const options: { [name: string]: DrillModel } = {};
    drills
      .filter((drill) => !term || drill.name.includes(term))
      .forEach((drill) => {
        if (options[drill.name]) return;
        options[drill.name] = drill;
      });
    return Object.values(options);
  }
}
