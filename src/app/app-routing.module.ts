import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    redirectTo: "history",
    pathMatch: "full",
  },
  {
    path: "history",
    loadChildren: () =>
      import("./history/history.module").then((m) => m.HistoryPageModule),
  },
  {
    path: "drill/:id",
    loadChildren: () =>
      import("./drill/drill.module").then((m) => m.DrillPageModule),
  },
  {
    path: "settings",
    loadChildren: () =>
      import("./settings/settings.module").then((m) => m.SettingsPageModule),
  },
  {
    path: "workouts",
    loadChildren: () =>
      import("./workouts/workouts.module").then((m) => m.WorkoutsPageModule),
  },
  {
    path: "workout/:id",
    loadChildren: () =>
      import("./workout/workout.module").then((m) => m.WorkoutPageModule),
  },
  {
    path: "charts",
    loadChildren: () =>
      import("./charts/charts.module").then((m) => m.ChartsPageModule),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
