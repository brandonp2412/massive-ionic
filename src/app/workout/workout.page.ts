import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { v4 as uuid } from "uuid";
import { DrillService } from "../drill/drill.service";
import { WorkoutModel } from "../workouts/workout.model";
import { DrillReportModel } from "./drill-report.model";

@Component({
  selector: "app-workout",
  templateUrl: "./workout.page.html",
  styleUrls: ["./workout.page.scss"],
})
export class WorkoutPage implements OnInit {
  workout = new WorkoutModel();
  viewMode = true;
  drillOptions: string[] = [];
  drillReports: DrillReportModel[] = [];
  isFavorite = false;

  constructor(
    private storage: Storage,
    private activatedRoute: ActivatedRoute,
    private drillService: DrillService,
    private router: Router,
  ) {}

  async ngOnInit() {
    this.drillOptions = await this.drillService.names();
    const id = this.activatedRoute.snapshot.paramMap.get("id");
    if (id === "new") return (this.viewMode = false);
    const workouts: WorkoutModel[] = await this.storage.get("workouts");
    this.workout = workouts.find((workout) => workout.id === id);
    const favorite = await this.storage.get("favorite");
    this.isFavorite = this.workout.id === favorite;
    this.drillReports = await this.drillService.report(this.workout.drills);
  }

  async save() {
    let workouts: WorkoutModel[] = await this.storage.get("workouts");
    if (this.workout.id) {
      workouts = workouts.map((oldWorkout) => {
        if (oldWorkout.id !== this.workout.id) return oldWorkout;
        return this.workout;
      });
    } else {
      workouts.push({ ...this.workout, id: uuid() });
    }
    await this.storage.set("workouts", workouts);
    this.router.navigate(["/workouts"]);
  }

  async toggleFavorite() {
    if (!this.isFavorite) await this.storage.set("favorite", this.workout.id);
    else {
      await this.storage.remove("favorite");
    }
    this.isFavorite = !this.isFavorite;
  }
}
