import { NgModule } from "@angular/core";
import { SelectAllDirective } from "./select-all.directive";

@NgModule({
  declarations: [SelectAllDirective],
  exports: [SelectAllDirective],
})
export class DirectivesModule {}
