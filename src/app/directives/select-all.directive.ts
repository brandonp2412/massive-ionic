import { Directive, ElementRef, HostListener } from "@angular/core";

@Directive({
  selector: "[select-all]",
})
export class SelectAllDirective {
  constructor(private el: ElementRef) {}

  @HostListener("click")
  selectAll() {
    const element: HTMLInputElement = this.el.nativeElement.querySelector(
      "input",
    );
    element.setSelectionRange(0, element.value.length);
    element.select();
  }
}
