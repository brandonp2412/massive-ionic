import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { PipesModule } from "../pipes/pipes.module";
import { HistoryPageRoutingModule } from "./history-routing.module";
import { HistoryPage } from "./history.page";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HistoryPageRoutingModule,
    PipesModule,
  ],
  declarations: [HistoryPage],
})
export class HistoryPageModule {}
