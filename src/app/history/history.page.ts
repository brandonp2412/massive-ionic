import { Component, OnInit, ViewChild } from "@angular/core";
import {
  AlertController,
  IonInfiniteScroll,
  IonRefresher,
  LoadingController,
} from "@ionic/angular";
import { Storage } from "@ionic/storage";
import * as moment from "moment";
import DrillModel from "../drill/drill.model";
import { DrillService } from "../drill/drill.service";
import { SettingsModel } from "../settings/settings.model";
import { SettingsService } from "../settings/settings.service";

@Component({
  selector: "app-history",
  templateUrl: "./history.page.html",
  styleUrls: ["./history.page.scss"],
})
export class HistoryPage implements OnInit {
  drills: DrillModel[] = [];
  limit = 10;
  offset = 0;
  term = "";
  loading = false;
  favorite: string;
  settings: SettingsModel;

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    private drillService: DrillService,
    private alertController: AlertController,
    private loadingController: LoadingController,
    private storage: Storage,
    private settingsService: SettingsService,
  ) {}

  ngOnInit() {}

  async ionViewWillEnter() {
    this.offset = 0;
    try {
      this.loading = true;
      this.term = "";
      await Promise.all([
        this.setDrills(),
        this.setFavorite(),
        this.setSettings(),
      ]);
    } finally {
      this.loading = false;
    }
  }

  async setFavorite() {
    this.favorite = await this.storage.get("favorite");
  }

  async setSettings() {
    this.settings = await this.settingsService.get();
  }

  async setDrills(event?: { target: IonRefresher }) {
    try {
      const newDrills = await this.drillService.get({
        limit: this.limit,
        offset: this.offset,
        term: this.term,
      });
      if (this.offset === 0) this.drills = newDrills;
      else this.drills.push(...newDrills);
    } finally {
      if (event) event.target.complete();
    }
    console.log({ drills: this.drills });
  }

  async remove(id: string) {
    console.log(`Removing drill ${id}`);
    this.offset = 0;
    try {
      this.loading = true;
      await this.drillService.remove(id);
      await this.setDrills();
    } finally {
      this.loading = false;
    }
  }

  async search() {
    this.offset = 0;
    try {
      this.loading = true;
      await this.setDrills();
    } finally {
      this.loading = false;
    }
  }

  async loadMore(event?: { target: IonRefresher }) {
    console.log("Loading more history...");
    this.offset++;
    await this.setDrills(event);
  }

  async refresh(event?: { target: IonRefresher }) {
    this.offset = 0;
    await this.setDrills(event);
  }

  trackDrill(_index: number, drill: DrillModel) {
    return drill.id;
  }

  async download() {
    console.log("Downloading...");
    try {
      this.loading = true;
      await this.drillService.download();
    } finally {
      this.loading = false;
    }
  }

  async confirmUpload() {
    const alert = await this.alertController.create({
      header: "Upload destructively",
      subHeader: "Replace all records with a saved file",
      message: "This action is not reversible.",
      buttons: [
        {
          text: "Ok",
          handler: async () => await this.upload(),
        },
        {
          text: "Cancel",
          role: "cancel",
        },
      ],
    });
    await alert.present();
  }

  isToday(date: string) {
    return moment(date).isSame(moment(), "day");
  }

  private async upload() {
    const loading = await this.loadingController.create({
      message: "Uploading drills...",
      translucent: true,
      backdropDismiss: true,
    });
    try {
      await this.drillService.upload();
      await loading.present();
    } finally {
      await loading.dismiss();
    }
    this.offset = 0;
    await this.setDrills();
  }
}
